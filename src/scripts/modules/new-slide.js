class NewSlide {
    constructor() {
        console.log('>>> New Slide constructor');

        // Call methods
        this.init();
    }

    init() {
        new Swiper('.swiper-container', {
            speed: 400,
            spaceBetween: 0,
            slidesPerView: 1,
            nextButton: ".opa",
            pagination: '.swiper-pagination',
            paginationClickable: true,
            // Responsive breakpoints
              breakpoints: {
                // when window width is <= 480px
                767: {
                  slidesPerView: 1,
                  spaceBetween: 30
                },
                920: {
                  slidesPerView: 1,
                  spaceBetween: 30
                }
              }
        });   

    }
}

export default NewSlide;
