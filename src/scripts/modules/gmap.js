var GoogleMapsLoader = require('google-maps');

class GMap {
    constructor() {
        console.log('>>> New Map constructor');

        // Call methods
        this.init();
    }

    init() {
        let pos = {lat: -8.061349, lng: -34.908145}; 
        GoogleMapsLoader.KEY = 'AIzaSyBX4xYzaIAu5BDIc8eerc4qUz00XzOQZ_c';

        GoogleMapsLoader.load(function(google) {
            let map = new google.maps.Map(document.getElementById('map'), {
              zoom: 19,
              center: pos,
              disableDefaultUI: true,
              scrollwheel: false,
            });
            let marker = new google.maps.Marker({
              position: pos,
              map: map
            });
        });


    }
}

export default GMap;
