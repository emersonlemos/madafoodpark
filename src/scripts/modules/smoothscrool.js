class SmoothScroll {
    constructor() {
        console.log('>>> New SmoothScroll');

        // Call methods
        this.init();
    }

    init() {
      $('.smoothscroll').on('click', function (e) {
    
      e.preventDefault();

      var target = this.hash,
        $target = $(target);

        $('html, body').stop().animate({
          'scrollTop': $target.offset().top - 0
        }, 800, 'swing', function () {
          window.location.hash = target;
        });

      });  


    }
}

export default SmoothScroll;
